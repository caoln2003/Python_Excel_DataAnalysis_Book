# -*- coding: utf-8 -*-

#【案例9-1】绘制中国银行2015年至2019年营业收入图表
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])


#【案例9-2】绘制中国银行和建设银行2015年至2019年营业收入图表
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (8,3))
#当前画布被分成1行2列，并开始绘制第一个图表
ax1 = plt.subplot(1, 2, 1)
plt.plot(df.columns[1:6], df.iloc[0,1:6], color = "red")
#当前画布被分成1行2列，并开始绘制第二个图表
ax2 = plt.subplot(1, 2, 2)
plt.plot(df.columns[1:6], df.iloc[1,1:6], color = "green")


#【案例9-3】以坐标系方式绘制中国银行和建设银行2015年至2019年营业收入图表
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
#创建1行2列的多窗口画布，并指定画布大小
fig, ax = plt.subplots(1, 2, figsize = (9,3))
#在[0]坐标戏中绘制中国银行的图表
ax[0].plot(df.columns[1:6], df.iloc[0,1:6], color = "red")
#在[1]坐标戏中绘制建设银行的图表
ax[1].plot(df.columns[1:6], df.iloc[0,1:6], color = "green")


#【案例9-4】设置坐标轴标题
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")


#【案例9-5】多窗口画布设置坐标轴标题
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
#创建1行2列的多窗口画布，并指定画布大小
fig, ax = plt.subplots(1, 2, figsize = (9,3))
#在[0]坐标戏中绘制中国银行的图表
ax[0].plot(df.columns[1:6], df.iloc[0,1:6], color = "red")
ax[0].set_xlabel("年份")
ax[0].set_ylabel("营业收入（亿元）")
#在[1]坐标戏中绘制建设银行的图表
ax[1].plot(df.columns[1:6], df.iloc[0,1:6], color = "green")
ax[1].set_xlabel("年份")
ax[1].set_ylabel("营业收入（亿元）")


#【案例9-6】设置坐标轴刻度
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
#X轴值刻度值文本
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
#设置X轴刻度值为xtitle中文字
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")


#【案例9-7】设置图表标题
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入变化", fontsize = "14")


#【案例9-8】多窗口画布设置图表标题
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
#创建1行2列的多窗口画布，并指定画布大小
fig, ax = plt.subplots(1, 2, figsize = (9,3))
xtitle = ["", "2015年", "2016年", "2017年", "2018年", "2019年"]
#在[0]坐标系中绘制中国银行的图表
ax[0].plot(df.columns[1:6], df.iloc[0,1:6], color = "red")
ax[0].set_xticklabels(xtitle)
ax[0].set_xlabel("年份")
ax[0].set_ylabel("营业收入（亿元）")
ax[0].set_title("中国银行")
#在[1]坐标系中绘制建设银行的营业收入图表
ax[1].plot(df.columns[1:6], df.iloc[0,1:6], color = "green")
ax[1].set_xticklabels(xtitle)
ax[1].set_xlabel("年份")
ax[1].set_ylabel("营业收入（亿元）")
ax[1].set_title("建设银行")


#【案例9-9】添加图例
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize = (4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6], color = "red", label = "中国银行")
plt.plot(df.columns[1:6], df.iloc[1,1:6], color = "green", label = "建设银行")
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.legend(loc = "upper left")


#【案例9-10】设置数据标签
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入变化", fontsize = "14")
#在2017年营业收入的数值处显示4833，即该年的营业收入额
plt.text(2017, 4833, 4833, va = "top", fontsize = 12)

#添加所有数据标签
plt.figure(figsize=(4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入变化", fontsize = "14")
for x, y in zip(df.columns[1:6], df.iloc[0,1:6]):
    plt.text(x, y, y, va = "top", fontsize = 12)
    

#【案例9-11】保存为图片
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6])
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入变化", fontsize = "14")
#在2017年营业收入的数值处显示4833，即该年的营业收入额
plt.text(2017, 4833, 4833, va = "top", fontsize = 12)
plt.savefig("D:/DataAnalysis/Chapter09Data/Revenue.jpg")


#【案例9-12】绘制中国银行2015年至2019年营业收入折线图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6], 
         color = "r", linestyle = "solid", marker = "o")
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入折线图", fontsize = "14")


#【案例9-13】绘制中国银行2015年至2019年营业收入散点图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.scatter(df.columns[1:6], df.iloc[0,1:6], 
            c = "r", s = 100, marker = "*")
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入散点图", fontsize = "14")


#【案例9-14】绘制中国银行2015年至2019年营业收入柱状图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.bar(df.columns[1:6], df.iloc[0,1:6], color = "b", width = 0.6)
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入柱状图", fontsize = "14")


#【案例9-15】绘制中国银行和建设银行2015年至2019年营业收入柱状图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(5,3))
import numpy as np
#设置X轴刻度值
xtitle = [2015, 2016, 2017, 2018, 2019]
xdata = np.arange(5)+1
plt.bar(xdata, df.iloc[0,1:6], 
        color = "b", width = 0.3, label = "中国银行")
plt.bar(xdata + 0.3, df.iloc[1,1:6], 
        color = "r", width = 0.3, label = "建设银行")
plt.xticks(xdata + 0.15, xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行和建设银行营业收入柱状图", fontsize = "14")
plt.legend(loc = "upper left")


#【案例9-16】绘制学生成绩直方图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/StudentScore.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.hist(df["成绩"],10)
plt.xlabel("成绩", fontsize = "12")
plt.ylabel("人数", fontsize = "12")
plt.title(label = "学生成绩直方图", fontsize = "14")


#【案例9-17】绘制中国银行2015年至2019年营业收入条形图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
plt.barh(df.columns[1:6], df.iloc[0,1:6], color = "b", height = 0.6)
ytitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.yticks(df.columns[1:6], xtitle)
plt.ylabel("年份", fontsize = "12")
plt.xlabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入条形图", fontsize = "14")


#【案例9-18】绘制中国银行2015年至2019年营业收入饼图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.pie(df.iloc[0,1:6], explode = [0,0,0,0,0.1], labels = xtitle, 
        autopct = "%.2f%%", radius = 1.5, shadow = True)


#【案例9-19】绘制中国银行2015年至2019年营业收入气泡图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,3))
import numpy as np
plt.scatter(df.columns[1:6], df.iloc[0,1:6], 
            c = "r", s = np.array(df.iloc[0,1:6].tolist())/5, marker = "o")
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("营业收入（亿元）", fontsize = "12")
plt.title(label = "中国银行营业收入气泡图", fontsize = "14")


#【案例9-20】绘制某公司营业收入明细雷达图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/RevenueDetail.xlsx")
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,4))
#雷达图中每个点的说明文字
labels = df["营业收入分项"]
#雷达图共分成6个点
dataLenth = 6
#雷达图中每个点的数据
data = df["金额"]
#设置雷达图中每个点的角度
angles = np.linspace(0, 2*np.pi, dataLenth, endpoint=False) # 分割圆周长
data = np.concatenate((data, [data[0]])) 
angles = np.concatenate((angles, [angles[0]])) 
plt.polar(angles, data, marker = "o") 
plt.xticks(angles, labels)
plt.tick_params('y', labelleft=False)
plt.title(label = "营业收入明细雷达图", fontsize = "14")


#【案例9-21】绘制中国银行、建设银行、农业银行、工商银行、交通银行2015年至2019年营业收入热力图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(4,4))
plt.imshow(np.array(df.iloc[0:5,1:6]), cmap = plt.cm.hsv)
plt.xticks([0,1,2,3,4], ["2015年", "2016年", "2017年", "2018年", "2019年"])
plt.yticks([0,1,2,3,4], df.iloc[:,0])
plt.title(label = "营业收入热力图", fontsize = "14")


#【案例9-22】绘制中国银行、建设银行、农业银行、工商银行2015年至2019年营业收入组合图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(5,3))
plt.plot(df.columns[1:6], df.iloc[0,1:6], color = "r", 
         linestyle = "solid", marker = "o", label = "中国银行")
plt.plot(df.columns[1:6], df.iloc[1,1:6], color = "b", 
         linestyle = "dashed", marker = "s", label = "建设银行")
plt.plot(df.columns[1:6], df.iloc[2,1:6], color = "g", 
         linestyle = "dashdot", marker = "*", label = "农业银行")
plt.plot(df.columns[1:6], df.iloc[3,1:6], color = "c", 
         linestyle = "dotted", marker = "h", label = "工商银行")
xtitle = ["2015年", "2016年", "2017年", "2018年", "2019年"]
plt.xticks(df.columns[1:6], xtitle)
plt.title(label = "营业收入组合图", fontsize = "14")
plt.legend()



#import matplotlib.pyplot as plt
#styles = plt.style.available
#x = [1,2,3,4,5,6,7,8,9,10]
#y = [10,14,12,18,16,22,20,26,24,28]
#for style in styles:
#    plt.figure()
#    plt.style.use(style)
#    plt.plot(x, y)
#    plt.title(style)
#    plt.savefig("D:/DataAnalysis/Chapter09Data/pic/" + str(style) + ".jpg")
  

#【案例9-23】绘制中国银行2015年至2019年营业收入柱状图
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/Revenue.xlsx")
import seaborn as sns
#定义主题风格，解决seaborn中文显示问题
sns.set_style('whitegrid',{'font.sans-serif':['simhei','Arial']}) 
#显示柱状图
ax = sns.barplot(x = df.columns[1:6], y = df.iloc[0,1:6])
#设置X轴标题，Y轴标题，图表标题
ax.set(xlabel="年份", ylabel="营业收入（亿元）", title = "中国银行营业收入柱状图")
#fig = ax.get_figure()
#fig.savefig("D:/DataAnalysis/Chapter09Data/01.jpg")


#【案例9-24】2010年至2019年GDP数据可视化
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter09Data/GDP.xlsx")
print(df)
import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False
plt.figure(figsize=(7,3))
#GDP柱状图
#提取列名作为X轴的显示标签
xlabels = df.columns[1:11]
#提取国内生产总值数据作为图表数据
values = df.iloc[1,1:11]
plt.bar(xlabels, values)
plt.xlabel("年份", fontsize = "12")
plt.ylabel("国内生产总值（亿元）", fontsize = "12")
plt.title(label = "2010年至2019年GDP变化", fontsize = "14")
#plt.savefig("D:/DataAnalysis/Chapter09Data/11.jpg")

#第一产业、第二产业、第三产业增加值组合图
#提取列名作为X轴的显示标签
xlabels = df.columns[1:11]
#提取第一产业数据
fvalues = df.iloc[2,1:11]
#提取第二产业数据
svalues = df.iloc[3,1:11]
#提取第三产业数据
tvalues = df.iloc[4,1:11]
plt.plot(xlabels, fvalues, color = "r", 
         linestyle = "solid", marker = "o", label = "第一产业")
plt.plot(xlabels, svalues, color = "b", 
         linestyle = "dashed", marker = "s", label = "第二产业")
plt.plot(xlabels, tvalues, color = "g", 
         linestyle = "dashdot", marker = "h", label = "第三产业")
plt.title(label = "第一产业、第二产业、第三产业增加值组合图", fontsize = "14")
plt.legend(loc = "upper left")
#plt.savefig("D:/DataAnalysis/Chapter09Data/22.jpg")

#2019年第一产业、第二产业、第三产业增加值饼图
#提取2010年数据
data1 = df.iloc[2:5,1]
#提取2019年数据
data2 = df.iloc[2:5,10]
#当前画布被分成1行2列，并开始绘制第一个饼图
ax1 = plt.subplot(1, 2, 1)
plt.pie(data1, labels = ["第一产业","第二产业","第三产业"], 
        autopct = "%.2f%%", explode = [0.01,0.01,0.01])
plt.title(label = "2010年", fontsize = "14")
#开始绘制第二个饼图
ax2 = plt.subplot(1, 2, 2)
plt.pie(data2, labels = ["第一产业","第二产业","第三产业"], 
        autopct = "%.2f%%", explode = [0.01,0.01,0.01])
plt.title(label = "2019年", fontsize = "14")
#plt.savefig("D:/DataAnalysis/Chapter09Data/33.jpg")

