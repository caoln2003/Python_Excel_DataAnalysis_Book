# -*- coding: utf-8 -*-

#【案例8-1】获取当前日期时间数据
from datetime import datetime
print("当前日期和时间：" + str(datetime.now()))
print("当前日期：" + str(datetime.now().date()))
print("当前时间：" + str(datetime.now().time()))
print("当前年：" + str(datetime.now().year))
print("当前月：" + str(datetime.now().month))
print("当前日：" + str(datetime.now().day))
print("当前星期几：" + str(datetime.now().weekday() + 1))
print("当前日期：" + datetime.now().date().strftime("%Y/%m/%d"))
print("当前时间：" + datetime.now().time().strftime("%I:%M:%S"))
print("当前星期几：" + datetime.now().strftime("%A"))


#【案例8-2】日期时间类型与字符串类型转换
from datetime import datetime
print("当前日期和时间：" + str(datetime.now()))
#格式化输出当前日期，日期分隔符为“/”
print("当前日期：" + datetime.now().date().strftime("%Y/%m/%d"))

#字符串转换为日期时间类型
from dateutil.parser import parse
from datetime import datetime
print(parse("2020-9-19"))
print(datetime.strptime("2020-9-19", "%Y-%m-%d"))


#【案例8-3】时间偏移计算
import pandas as pd
from datetime import datetime
from datetime import timedelta
df = pd.read_excel("D:/DataAnalysis/Chapter08Data/Log.xlsx")
delta = df.iloc[3,0] - df.iloc[0,0]
print("间隔天数：" + str(delta.days))
print("间隔小时数：" + str(delta.seconds / 3600))

#判断用户Fly在2019年9月13日登录后24小时内是否再次登录
newdate = df.iloc[2,0] + timedelta(days = 1)
if (newdate - df.iloc[5,0]).microseconds > 0:
    print("24小时内再次登录")
else:
    print("24小时内没有再次登录")
   

#【案例8-4】股票数据时间序列分析
import pandas as pd
from datetime import datetime
from datetime import timedelta
df = pd.read_excel("D:/DataAnalysis/Chapter08Data/Stock.xlsx")
df["交易日期"] = df["交易日期"].astype("datetime64[ns]")
df.set_index("交易日期", inplace = True)

#获取2019年的股票数据
print(df["2019"])

#获取2020年1月的股票数据
print(df["2020/01"])

#获取2020年1月2日至2020年1月6日的股票数据
print(df["2020/01/02":"2020/01/06"])

#通过时间偏移计算获取2019年12月27日后第6天即2020年1月2日的股票价格
newdate = datetime(2019,12,27) + timedelta(days = 6)
print("2020年1月2日收盘价格：" + str(df.loc[newdate,"收盘价"]))

