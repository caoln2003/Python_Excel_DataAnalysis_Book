# python_excel_dataAnalysis_book

#### 介绍
提供由曹鉴华，赵奇编著的《数据荒岛求生--对比Excel，轻松学习Python数据分析》一书各章节代码和数据素材

#### 使用说明

1.  Excel版本：2016版本及以上
2.  Python开发工具：Anaconda Spyder、Anaconda JupyterNotebook
3.  Python开发依赖第三方库：numpy、pandas、matplotlib、seaborn

#### 联系作者

邮箱：caojh@tust.edu.cn
微信：cao412308234
微信公众号：稻谷编程
知乎专栏：稻谷团Python人工智能
CSDN博客：blog.csdn.net/caojianhua2018

