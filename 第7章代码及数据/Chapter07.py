# -*- coding: utf-8 -*-

#【案例7-1】财务报销数据分组
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter07Data/Account.xlsx")
df_g = df.groupby("报销部门")
print(df_g)

#查看“报销部门”的分组情况
print(df_g.groups)

#查看“科技部”的分组数据值
print(df_g.get_group("科技部"))

#汇总每个部门的报销金额总和
print(df_g.sum())

#指定对“金额（元）”列进行汇总求和
print(df_g["金额（元）"].sum())

#按照报销部门和费用项目两列进行分组
df_g2 = df.groupby(["报销部门","费用项目"])
print(df_g2.groups)

#按照分组情况汇总报销金额总和
print(df_g2.sum())


#【案例7-2】财务报销数据分类统计
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter07Data/Account.xlsx")
df_g = df.groupby("报销部门")
#先汇总统计个数再汇总求和
print(df_g.agg(["count","sum"]))

#对“报销人”列选择count汇总方式，对“金额（元）”列选择sum汇总方式
print(df_g.agg({"报销人":"count", "金额（元）":"sum"}))


#【案例7-3】使用数据透视表分析财务报销数据
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter07Data/Account.xlsx")
##统计相同部门中相同费用项目的报销人数
df_pivot = df.pivot_table(values = "报销人", index = "报销部门", 
                          columns = "费用项目", aggfunc = "count")
print(df_pivot)

#显示合计信息
df_pivot = df.pivot_table(values = "报销人", index = "报销部门", 
                          columns = "费用项目", aggfunc = "count",
                          margins = True)
print(df_pivot)

#修改合计信息的显示名称
df_pivot = df.pivot_table(values = "报销人", index = "报销部门", 
                          columns = "费用项目", aggfunc = "count",
                          margins = True, margins_name = "合计")
print(df_pivot)

#指定各列汇总方式
df_pivot = df.pivot_table(values = ["报销人", "金额（元）"], index = "报销部门", 
                          columns = "费用项目", 
                          aggfunc = {"报销人":"count", "金额（元）":"sum"},
                          margins = True, margins_name = "合计")
print(df_pivot)


#【案例7-4】上海市二类疫苗采购数据分组汇总
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter07Data/Vaccine.csv")
#按照“疫苗名称”进行分组，统计每组疫苗生产厂家数量，以及价格平均值
df_g = df.groupby("疫苗名称")
print(df_g.agg({"生产厂家":"count", "价格":"mean"}))

#按照“疫苗名称”进行分组，统计每组疫苗中国产疫苗和进口疫苗的数量，以及国产疫苗和进口疫苗的最高价格
df_pivot = df.pivot_table(values = ["生产厂家", "价格"], index = "疫苗名称", 
                          columns = "来源", 
                          aggfunc = {"生产厂家":"count", "价格":"max"})
print(df_pivot)