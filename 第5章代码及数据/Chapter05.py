# -*- coding: utf-8 -*-

#【案例5-1】选择一行数据
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter05Data/PM25BJ2.xlsx")
#用后一个数据替换缺失值
df = df.fillna(method = "backfill", axis = 1)
#对于最后一列使用前一个数据替换缺失值
df = df.fillna(method = "ffill", axis = 1)
#修改部分列的数据类型
df["datehour"] = df["datehour"].astype(str)
df["date"] = df["date"].astype("datetime64[ns]")
df["hour"] = df["hour"].astype("int64")
#设置第一列“date”列为行索引
df.set_index("datehour", inplace = True)
print(df.head(5))

#使用标签索引方式，获取2020年2月1日8时各观测站点的数据
df_onerow = df.loc["202002018"]
#使用行号索引方式，获取2020年2月1日8时各观测站点的数据
df_onerow = df.iloc[8]
print(df_onerow)


#【案例5-2】选择一列数据
#使用标签索引方式，获取“东四”观测站点的数据
df_onecolumn = df["东四"]
#使用列号索引方式，获取“东四”观测站点的数据
df_onecolumn = df.iloc[:,2]
print(df_onecolumn)


#【案例5-3】选择多行数据
#使用标签索引方式选择连续行，获取2020年2月1日8时至12时各观测站点的数据
df_morerow = df.loc["202002018":"2020020112"]
#使用行号索引方式选择连续行，获取2020年2月1日8时至12时各观测站点的数据
df_morerow = df.iloc[8:13]
print(df_morerow)

#使用标签索引方式选择不连续行，获取2020年2月1日8时、10时、12时各观测站点的数据
df_morerow = df.loc[["202002018", "2020020110", "2020020112"]]
#使用行号索引方式选择不连续行，获取2020年2月1日8时、10时、12时各观测站点的数据
df_morerow = df.iloc[[8, 10, 12]]
print(df_morerow)


#【案例5-4】选择多列数据
#使用标签索引方式选择连续列，获取“东四”至“奥体中心”5个观测站点的数据
df_morecolumn = df.loc[:, "东四":"奥体中心"]
#使用列号索引方式选择连续列，获取“东四”至“奥体中心”5个观测站点的数据
df_morecolumn = df.iloc[:, 2:7]
print(df_morecolumn)

#使用标签索引方式选择不连续列，获取“东四”、“官园”、“奥体中心”3个观测站点的数据
df_morecolumn = df.loc[:, ["东四", "官园", "奥体中心"]]
#使用列号索引方式选择不连续列，获取“东四”、“官园”、“奥体中心”3个观测站点的数据
df_morecolumn = df.iloc[:, [2, 4, 6]]
print(df_morecolumn)


#【案例5-5】普通索引方式选取区域数据
#使用标签索引方式选择连续行列区域，获取2020年2月1日8时至10时，“东四”至“奥体中心”5个观测站点的数据
df_rowcolumn = df.loc["202002018":"2020020110", "东四":"奥体中心"]
print(df_rowcolumn)
#使用标签索引方式选择不连续行列区域，获取2020年2月1日8时、10时、12时，“东四”、“官园”、“奥体中心”3个观测站点的数据
df_rowcolumn = df.loc[["202002018", "2020020110", "2020020112"], ["东四", "官园", "奥体中心"]]
print(df_rowcolumn)


#【案例5-6】切片索引方式选取区域数据
#使用切片索引方式选择行列区域，获取2020年2月1日8时至10时“东四”至“奥体中心”5个观测站点的数据
df_rowcolumn = df.iloc[8:13, 2:7]
print(df_rowcolumn)
#使用切片索引方式选择不连续行列区域，获取2020年2月1日8时、10时、12时，“东四”、“官园”、“奥体中心”3个观测站点的数据
df_rowcolumn = df.iloc[[8, 10, 12], [2, 4, 6]]
print(df_rowcolumn)


#【案例5-7】普通索引与切片索引混合使用
#使用普通索引与切片索引混合方式，获取2020年2月1日8时至12时，“东四”、“官园”、“奥体中心”3个观测站点的数据
df_rowcolumn = df.ix[8:13, ["东四", "官园", "奥体中心"]]
print(df_rowcolumn)


#【案例5-8】横向合并多表数据
import pandas as pd
df_daily = pd.read_excel("D:/DataAnalysis/Chapter05Data/Stock.xlsx", 
                         sheet_name = "daily")
df_dailybasic = pd.read_excel("D:/DataAnalysis/Chapter05Data/Stock.xlsx", 
                              sheet_name = "dailybasic")
df_stockname = pd.read_excel("D:/DataAnalysis/Chapter05Data/Stock.xlsx", 
                             sheet_name = "stockname")
print(df_daily.head(5))
print(df_dailybasic.head(5))
print(df_stockname.head(5))
#横向拼接
df_dailydata = pd.merge(df_daily, df_dailybasic)
print(df_dailydata)

#指定连接键为“股票代码”列
df_dailydata = pd.merge(df_daily, df_dailybasic, on = "股票代码")
print(df_dailydata)

#内连接
df_dailyall = pd.merge(df_daily, df_stockname, on = "股票代码", how = "inner")
print(df_dailyall)

#左连接
df_dailyall = pd.merge(df_daily, df_stockname, on = "股票代码", how = "left")
print(df_dailyall)

#右连接
df_dailyall = pd.merge(df_daily, df_stockname, on = "股票代码", how = "right")
print(df_dailyall)

#外连接
df_dailyall = pd.merge(df_daily, df_stockname, on = "股票代码", how = "outer")
print(df_dailyall)


#【案例5-9】纵向合并多表数据
import pandas as pd
df_daily = pd.read_excel("D:/DataAnalysis/Chapter05Data/Stock.xlsx", 
                         sheet_name = "daily")
df_daily2 = pd.read_excel("D:/DataAnalysis/Chapter05Data/Stock.xlsx", 
                         sheet_name = "daily2")
df_dailyall = pd.concat([df_daily, df_daily2])
print(df_dailyall)

#修改行索引为从0开始的顺序编号
df_dailyall = pd.concat([df_daily, df_daily2], ignore_index = True)
print(df_dailyall)

#删除重复数据
df_dailyall.drop_duplicates(inplace = True)

#重建索引编号
df_dailyall.reset_index(drop = True, inplace = True)
print(df_dailyall)


#【案例5-10】股票数据选择案例
import pandas as pd
df_daily = pd.read_excel("D:/DataAnalysis/Chapter05Data/StockData.xlsx", 
                         sheet_name = "daily")
df_dailybasic = pd.read_excel("D:/DataAnalysis/Chapter05Data/StockData.xlsx", 
                         sheet_name = "dailybasic")
df_moneyflow = pd.read_excel("D:/DataAnalysis/Chapter05Data/StockData.xlsx", 
                         sheet_name = "moneyflow")

print(df_daily.head(4))
print(df_dailybasic.head(4))
print(df_moneyflow.head(4))
print(df_daily.info())

#将三个表的日期列数据类型设置为字符串型，并设置为索引列
df_daily["交易日期"] = df_daily["交易日期"].astype(str)
df_daily.set_index("交易日期", inplace = True)
df_dailybasic["交易日期"] = df_dailybasic["交易日期"].astype(str)
df_dailybasic.set_index("交易日期", inplace = True)
df_moneyflow["交易日期"] = df_moneyflow["交易日期"].astype(str)
df_moneyflow.set_index("交易日期", inplace = True)

#选取2020年8月3日至8月7日的收盘价、换手率和净流入额数据
df_dailyfw = df_daily.loc["20200803":"20200807", "收盘价"]
df_dailybasicfw = df_dailybasic.loc["20200803":"20200807", "换手率"]
df_moneyflowfw = df_moneyflow.loc["20200803":"20200807", "净流入额"]
df_fw = pd.merge(df_dailyfw, df_dailybasicfw, on = "交易日期")
print(df_fw)

print(df_moneyflowfw)

#使用左连接方式
df = pd.merge(df_fw, df_moneyflowfw, on = "交易日期", how = "left")
print(df)
#使用右连接方式
df = pd.merge(df_fw, df_moneyflowfw, on = "交易日期", how = "right")
print(df)