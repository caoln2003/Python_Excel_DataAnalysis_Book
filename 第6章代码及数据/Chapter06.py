# -*- coding: utf-8 -*-

#【案例6-1】四则运算
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter06Data/ComData.xlsx")
print(df)
df["data3"] = df["data1"] + df["data2"]
print(df)
df["data3"] = df["data1"] - df["data2"]
print(df)
df["data3"] = df["data1"] * df["data2"]
print(df)
df["data3"] = df["data1"] / df["data2"]
print(df)
df["data3"] = (df["data1"] + df["data2"]) * 2
print(df)
df["data1"] = df["data1"] + 10
print(df)


#【案例6-2】多表运算
import pandas as pd
df_com1 = pd.read_excel("D:/DataAnalysis/Chapter06Data/ComDataSheet.xlsx", 
                        sheet_name = "com1")
df_com2 = pd.read_excel("D:/DataAnalysis/Chapter06Data/ComDataSheet.xlsx", 
                        sheet_name = "com2")
df_com3 = pd.read_excel("D:/DataAnalysis/Chapter06Data/ComDataSheet.xlsx", 
                        sheet_name = "com3")
print(df_com1)
print(df_com2)
df_com3["data3"] = df_com1["data1"] + df_com2["data2"]
print(df_com3)


#【案例6-3】谁大谁小，返回True/False
import pandas as pd
df = pd.read_excel("D:/DataAnalysis/Chapter06Data/ComData.xlsx")
df["data3"] = df["data1"] > df["data2"]
print(df)

#【案例6-4】统计个数
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.count())
print(df.count(axis = 1))


#【案例6-5】求和计算
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].sum())
print(df.iloc[:,2:7].sum(axis = 1))


#【案例6-6】计算平均值
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].mean())
print(df.iloc[:,2:7].mean(axis = 1))


#【案例6-7】求最大/最小值
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].max(axis = 1))
print(df.iloc[:,2:7].min(axis = 1))


#【案例6-8】计算中位数
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].median(axis = 1))

#【案例6-9】计算众数
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].mode(axis = 1))


#【案例6-10】计算方差
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].var(axis = 1))


#【案例6-11】计算标准差
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].std(axis = 1))


#【案例6-12】计算相关系数
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/BJPM10.csv")
print(df.iloc[:,2:7].corr())

#【案例6-13】白酒质量评分数据计算案例
import pandas as pd
df = pd.read_csv("D:/DataAnalysis/Chapter06Data/winequality-white.csv", sep = ";")
print("统计个数：" + str(df["fixed acidity"].count()))
print("求平均值：" + str(df["fixed acidity"].mean()))
print("求最大值：" + str(df["fixed acidity"].max()))
print("求最小值：" + str(df["fixed acidity"].min()))
print("求中位数：" + str(df["fixed acidity"].median()))
print("求众数：" + str(df["fixed acidity"].mode()))
print("求方差：" + str(df["fixed acidity"].var()))
print("求标准差：" + str(df["fixed acidity"].std()))
print(df.iloc[:,[0,11]].corr())
